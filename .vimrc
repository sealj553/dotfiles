"curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
"    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
call plug#begin('~/.vim/plugged')
"Plug 'easymotion/vim-easymotion', { 'on': '<Plug>(easymotion-bd-w)' }
Plug 'ap/vim-buftabline'
Plug 'tpope/vim-eunuch'
"Plug 'tikhomirov/vim-glsl'
call plug#end()

"color settings
syntax on
hi Visual       ctermfg=0 
hi Search       ctermfg=190 ctermbg=0 cterm=UNDERLINE
hi MatchParen   ctermbg=237
hi LineNr       ctermfg=237
hi CursorLineNr ctermfg=241

"tabline colors
hi TabLineFill ctermfg=6 ctermbg=0 cterm=NONE
hi TabLine     ctermfg=6 ctermbg=0 cterm=NONE
hi TabLineSel  ctermfg=0 ctermbg=6 cterm=NONE

"statusline
set laststatus=0

"behavior
set autoindent
"keep 5 lines below and above the cursor
set scrolloff=5 
"tab = 4 spaces
set tabstop=4 softtabstop=4 shiftwidth=4 expandtab
set number relativenumber
"case insensitive search
set ignorecase
"disable new line comment
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
"use tabs in makefiles (doesn't it already do that?)
"autocmd FileType make setlocal noexpandtab

set timeoutlen=100 ttimeoutlen=0

"use system clipboard
set clipboard=unnamedplus
"use the clipboards of vim and win
"set clipboard+=unnamed  

"disable mouse 
imap <LeftMouse> <nop> 

"This unsets the last search pattern register by hitting enter
nnoremap <CR> :noh<CR>
nnoremap ; :

"buffer conf
set hidden
nnoremap <C-H> :bprev<CR>
nnoremap <C-L> :bnext<CR>
inoremap <C-H> <Esc>:bprev<CR>
inoremap <C-L> <Esc>:bnext<CR>

"persistant undo
set undofile 
set undodir=~/.vim/undodir

"goodbye ex mode
nnoremap Q <nop> 

"easymotion config
nmap <space> <Plug>(easymotion-bd-w)

"buftabline
"2 = show always
"1 = show when 2 or more buffers
let g:buftabline_show = 2 
"show if modified
let g:buftabline_indicators = 1

" vim-eunuch commands:
"    :Delete: Delete a buffer and the file on disk simultaneously.
"    :Unlink: Like :Delete, but keeps the now empty buffer.
"    :Move: Rename a buffer and the file on disk simultaneously.
"    :Rename: Like :Move, but relative to the current file's containing directory.
"    :Chmod: Change the permissions of the current file.
"    :Mkdir: Create a directory, defaulting to the parent of the current file.
"    :Cfind: Run find and load the results into the quickfix list.
"    :Clocate: Run locate and load the results into the quickfix list.
"    :Lfind/:Llocate: Like above, but use the location list.
"    :Wall: Write every open window. Handy for kicking off tools like guard.
"    :SudoWrite: Write a privileged file with sudo.
"    :SudoEdit: Edit a privileged file with sudo.
"    File type detection for sudo -e is based on original file name.
"    New files created with a shebang line are automatically made executable.
"    New init scripts are automatically prepopulated with /etc/init.d/skeleton.
