#!/bin/sh

active_sink=$(pacmd list-sinks | grep "* index: " | sed "s/[^0-9]*//g")
sinks=($(pacmd list-sinks | grep index | tr -d '' | sed "s/[^0-9]*//g"))
numSinks=${#sinks[@]}

if [[ $numSinks != 2 ]]; then
    exit
fi

if [[ ${sinks[0]} == $active_sink ]]; then
    #first is active
    newSink=${sinks[1]}
else
    #second is active
    newSink=${sinks[0]}
fi

pacmd set-default-sink $newSink

#change all audio streams to new sink
pactl list short sink-inputs|while read stream; do
    streamId=$(echo $stream | cut '-d ' -f1)
    #echo "moving stream $streamId"
    pactl move-sink-input "$streamId" "$newSink"
done
