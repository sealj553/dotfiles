#!/bin/sh
if [ $(/usr/bin/id -u) -ne 0 ]; then
    echo "Invalid Permissions. Are you root?"
    exit
fi

/home/jackson/scripts/backuplaptop.sh
/home/jackson/scripts/backuphdd.sh
/home/jackson/scripts/backuparchive.sh
