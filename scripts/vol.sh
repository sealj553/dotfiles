#!/bin/sh

#finds the active sink for pulse audio and increments the volume. useful when you have multiple audio outputs and have a key bound to vol-up and down

inc=5
maxvol=150
active_sink=$(pacmd list-sinks | awk '/* index:/{print $3}')

function volUp {
    #getCurVol
    curVol=$(pacmd list-sinks | grep -A 15 "* index:" | awk '/front-left/{print $5}' | tr -d '%')

    if (( curVol + inc < maxvol )); then
        pactl set-sink-volume ${active_sink} +${inc}%
    else
        pactl set-sink-volume ${active_sink} ${maxvol}%
    fi
}

function volDown {
    pactl set-sink-volume ${active_sink} -${inc}%
}

function volTogMute {
    #volMuteStatus
    curStatus=$(pacmd list-sinks | grep -A 15 "* index:" | awk '/muted/{print $2}')
    if [[ $curStatus == 'yes' ]]; then
        #unmute
        pactl set-sink-mute ${active_sink} 0
    else
        #mute
        pactl set-sink-mute ${active_sink} 1
    fi
}

case "$1" in
    --up)
        volUp
        ;;
    --down)
        volDown
        ;;
    --togmute)
        volTogMute
        ;;
esac
