#!/bin/sh
STR=$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep -E "state|percentage" | awk '{print $2}')
STATUS=$(echo ${STR} | awk '{print $1}')
PERCENT=$(echo ${STR} | awk '{print $2}')
PERCENTNUM=$(echo ${PERCENT} | sed 's/%//')

echo -n " <span font='Wuncon Siji 8' rise='1000'>"
if [ $STATUS == "charging" ] || [ $STATUS == 'fully-charged' ]; then
    echo -n ""
else
    if $(( $PERCENTNUM < 16 )); then
        echo -n "<span foreground='#FB4934'></span>"
    else
        echo -n ""
    fi
fi

#echo "</span>$PERCENT"
echo "</span>"
