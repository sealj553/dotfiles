#!/bin/sh

wid=$(wmctrl -l | grep dropdown | head -n1 | awk '{print substr($1,4)}')
#wid=$(xwininfo -root -tree | grep urxvtq | head -n1 | awk '{print $1}')
activeWindow=$(xprop -root | grep "_NET_ACTIVE_WINDOW(WINDOW)" | awk '{print substr($5,3)}')
#activeWindow=$(printf "0x%x\n" $(xdotool getactivewindow))

#if st is not the active window, make it so
if [ "$wid" != "$activeWindow" ]; then
    wmctrl -ia "0x$wid"
else
    wmctrl -ir "0x$wid" -b add,hidden
fi

#if st is not running, start it
if [ -z "$wid" ]; then
    st -t dropdown &
fi
