#!/bin/sh
if [ $(/usr/bin/id -u) -ne 0 ]; then
    echo "Invalid Permissions. Are you root?"
    exit
fi

rsync -aAXv --delete /run/media/jackson/SG5TB1/ /run/media/jackson/SG5TB2/
