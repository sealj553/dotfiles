#!/bin/sh

export DISPLAY=:0
export XAUTHORITY=/home/jackson/.Xauthority

false=0
true=1

laptop="LVDS1"
monitor1="DP3"
monitor2="HDMI2"

xrandr | grep "$monitor1 connected" &> /dev/null && mon1=$true || mon1=$false 
xrandr | grep "$monitor2 connected" &> /dev/null && mon2=$true || mon2=$false 

if ((mon1)); then
    if ((mon2)); then
        #both
        xrandr --output $monitor1 --mode 1920x1080 --rate 144.00 --primary
        xrandr --output $laptop --off
        xrandr --output $monitor2 --mode 1920x1080 --right-of $monitor1
    else
        #1 and not 2
        xrandr --output $monitor1 --mode 1920x1080 --rate 144.00 --primary
        xrandr --output $monitor2 --off
        xrandr --output $laptop --mode 1600x900 --right-of $monitor1
    fi
else
    if ((mon1)); then
        #2 and not 1
        xrandr --output $monitor2 --mode 1920x1080 --primary
        xrandr --output $monitor1 --off
        xrandr --output $laptop --mode 1600x900 --right-of $monitor2
    else
        #neither
        xrandr --output $monitor1 --off
        xrandr --output $laptop --mode 1600x900 --primary
        xrandr --output $monitor2 --off
    fi
fi
