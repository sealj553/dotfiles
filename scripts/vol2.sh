#!/bin/sh

active_sink=$(pacmd list-sinks | grep -A 15 "* index:")
curVol=$(echo "$active_sink" | awk '/front-left/{print $5}')
muted=$(echo "$active_sink" | awk '/muted/{print $2}')

if [[ $muted == 'yes' ]]; then
    #muted
    echo " <span font='Wuncon Siji 8' rise='500'></span>$curVol"
else
    #unmuted
    echo " <span font='Wuncon Siji 8' rise='500'></span>$curVol"
fi
