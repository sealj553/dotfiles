#!/bin/sh

title="Title: "$(cmus-remote -Q | grep title | cut -d' ' -f3-)
artist="Artist: "$(cmus-remote -Q | grep artist | cut -d' ' -f3-)
album="Album: "$(cmus-remote -Q | grep album | cut -d' ' -f3-)

notify-send "Now Playing" "$title\n$artist\n$album"
