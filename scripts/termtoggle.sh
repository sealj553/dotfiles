#!/bin/bash

wid=$(wmctrl -l | grep urxvtq | head -n1 | awk '{print $1}')
#wid=$(xwininfo -root -tree | grep urxvtq | head -n1 | awk '{print $1}')
activeWindow=$(xprop -root | grep "_NET_ACTIVE_WINDOW(WINDOW)" | awk '{print $5}')
#activeWindow=$(printf "0x%x\n" $(xdotool getactivewindow))

#if urxvtq is not the active window, make it so
if [[ $wid -ne $activeWindow ]]; then
    wmctrl -ia "$wid"
else
    wmctrl -ir $wid -b add,hidden
fi

#if urxvtq is not running, start it
if [ -z "$wid" ]; then
    urxvtc -name urxvtq &
fi
