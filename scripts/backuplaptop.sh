#!/bin/sh
if [ $(/usr/bin/id -u) -ne 0 ]; then
    echo "Invalid Permissions. Are you root?"
    exit
fi

rsync -aAXv --inplace --delete --exclude-from='/home/jackson/scripts/rsync_exclude_laptop.txt' / /run/media/jackson/ARCHIVE/Backups/thinkpad/
